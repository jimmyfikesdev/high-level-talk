// Breakpoints

@xsmall: 640px;
@small: 768px;
@medium: 960px;
@large: 1024px;
@desktop: 1366px;

//fonts
@import url('https://fonts.googleapis.com/css2?family=Anton&display=swap');

@import url('https://fonts.googleapis.com/css2?family=Anton&family=Montserrat:wght@300;500;700&display=swap');

// Global Styles

body {
    font-family: 'Montserrat', sans-serif;
    font-weight: 300;
}

a {
    &:hover {
        opacity: 0.7;
    }
}

h1, h2, h3, h4, h5, h6 {
    font-family: 'Anton', sans-serif;
}

.site-container {
    overflow: hidden;
}

.site-header {
    padding: 0 20px;
    .title-area {
        max-width: 70px!important;
    }
    .home & {
        box-shadow: none;
        border-bottom: 2px solid #f2f2f2;
        nav.nav-primary  {
            float: none;
            display: table;
            margin: 10px auto 0;
            @media(max-width: @medium) {
                display: none;
            }
            ul {
                li {
                    a {
                        color: #000!important; 
                    } 
                }
            }
        }
    } 
    nav.nav-primary  {
        margin-top: 10px;
        @media(max-width: @medium) {
            display: none;
        }
        ul {
            li {
                a {
                    font-size: 20px;
                    color: #000!important; 
                } 
            }
        }
    }
}

.site_title_container {
    margin-top: 0;
    text-align: center;
    padding: 20px;
    @media(max-width: @small) {
    }
    h1 {
        font-size: 60px;
        text-transform: uppercase;
        color: #f5ebeb;
        margin-bottom: 0;
        span {
            color: #000;
            @media(max-width: @medium) {
                display: block;
            }
            @media(max-width: @xsmall) {
                display: inline;
            }
            &.subtext {
                display: block;
                font-size: 16px;
                font-style: italic;
                font-family: 'Montserrat', sans-serif;
                text-transform: none;
            }
        }
    }
}

.featured_post {
        display: flex;
        align-items: baseline;
        flex-wrap: wrap;
        margin-top: 0;
        padding: 0 20px;
        .featured-column {
            &.featured_left {
                flex-basis: 60%;
                @media(max-width: @small) {
                    flex-basis: 100%;
                }
                img {
                    margin-bottom: 20px;
                }
                a {
                    color: #000;
                    text-decoration: none;
                    h2 {
                        font-size: 50px;
                    }
                }
            }
            &.featured_right {
                flex-basis: 40%;
                padding-left: 30px;
                @media(max-width: @small) {
                    flex-basis: 100%;
                    padding-left: 0;
                }
                h2 {
                    font-size: 20px;
                    border-bottom: 1px solid #000;
                    padding-bottom: 10px;
                }
                .pposts_container {
                    .ppost_image_wrapper {
                        img {
                            max-width: 180px;
                            float: right;
                            margin-left: 10px;
                            @media(max-width: @medium) {
                                float: none;
                                max-width: 100%;
                                margin-left: 0;
                                margin-bottom: 20px;
                            }
                        }
                    }
                    a {
                        color: #000;
                        text-decoration: none;
                    }
                    .ppost_content_wrapper {
                    }
                }
            }
        }
}

.home {
    #genesis-content {
        display: none;
    }
    .site-inner {
        max-width: 1100px;
        margin: 0 auto;
        padding: 0;
    }
    .logo_container {
        max-width: 500px;
        margin: 0 auto;
        padding: 30px 20px 10px;
        img {
            max-width: 100%;
        }
    }
    .menu-primary-menu-container {
        max-width: 600px;
        margin: 0 auto;
        display: flex;
        padding: 0 20px;
        .menu {
            display: flex;
            li {
                a  {
                    color: #000;
                    text-decoration: none;
                    text-transform: uppercase;
                }
            }
        }
    }
    .homepage_posts_container {
        margin: 20px auto 0;
        padding: 0 20px;
        .group {
            padding: 50px 0;
            &:nth-child(2n) {
                background: #f2f2f2;
                position: relative;
                &:before, &:after {
                    content: '';
                    position: absolute;
                    background: #f2f2f2;
                    width: 100%;
                    height: 100%;
                    top: 50%;
                    transform: translateY(-50%);
                }
                &:before {
                    right: 100%;
                }
                &:after {
                    left: 100%;
                }
            }
            .group-content {
                margin-bottom: 40px;
                &:last-of-type {
                    margin-bottom: 0;
                    p {
                        margin-bottom: 0;
                    }
                }
            }
            a {
                text-decoration: none;
                color: #000;
                h2 {
                    margin: 0 0 30px 0;
                    padding-bottom: 10px;
                    text-transform: uppercase;
                    border-bottom: 1px solid #00000014;
                    position: relative;
                    span.view_more {
                        font-size: 12px;
                        position: absolute;
                        top: 50%;
                        transform: translateY(-50%);
                        right: 0;
                        text-transform: none;
                    }
                }
                h4 {
                    margin: 0 0 10px 0;
                }
            }
            p {
                padding-left: 20px;
                @media(max-width: @small) {
                    padding-left: 0;
                } 
            }
        }
    }
}

// Archive page styles

.archive {
    .archive-description { 
        display: none;
        h1 {
            font-size: 40px;          
        }
    }
    .post_container {
        a {
            text-decoration: none;
            h3 {
                color: #000;
            }
        }
    }
}

.archive_title_container {
    background: #d9d9d936;
    padding: 50px 20px;
    .archive_title_container_wrap {
        max-width: 700px;
        margin: 0 auto;
        h1 {
           color: #000;
           font-size: 60px;
           font-weight: 700; 
           margin-bottom: 0;
        }
    }
}

.custom_footer {
    background: #000;
    
    p,a {
        color: #fff;
    }
}
