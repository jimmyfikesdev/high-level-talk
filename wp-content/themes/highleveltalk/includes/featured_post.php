<?php 

function featured_post_hero() {
    $fargs = array(
        'post_type' => 'post',
        'posts_per_page' => 1,  //show all posts
        'tag' => 'featured_post',

    );
$featured_posts = new WP_Query($fargs);
    $output = "";
    $output .= "<div class='featured_post'>";
if( $featured_posts->have_posts() ):
    
    while( $featured_posts->have_posts() ) : $featured_posts->the_post();

            $featured_image = get_the_post_thumbnail();
            $featured_title = get_the_title(); 
            $featured_excerpt = get_the_excerpt();
            $featured_link = get_the_permalink();

    
            $output .= "<div class='featured_left featured-column'>";
                $output .=  "<a href='{$featured_link}'>" .$featured_image . "</a>";
                $output .= "<a href='{$featured_link}'><h2>" . $featured_title . "</h2></a>";
                $output .= "<p>" . $featured_excerpt . "</p>";
            $output .= "</div>";
            
        

endwhile;
    wp_reset_postdata();
endif;

$pargs = array(
    'post_type' => 'post',
    'posts_per_page' => 3,  //show all posts
    'tag__not_in' => array(8),

);
$pposts = new WP_Query($pargs);
if( $pposts->have_posts() ):
    $output .= "<div class='featured_right featured-column'>";
    $output .=  "<h2>Related</h2>";
    while( $pposts->have_posts() ) : $pposts->the_post();
            $pimage = get_the_post_thumbnail();
            $ptitle = get_the_title(); 
            $pexcerpt = get_the_excerpt();
            $plink = get_the_permalink();
            
            $output .= "<div class='pposts_container'>";
                $output .= "<div class='ppost_image_wrapper'>";
                    $output .= "<a href='{$plink}'>" . $pimage . "</a>";
                $output .= "</div>";
                $output .= "<a href='{$plink}'><h3>" . $ptitle . "</h3></a>";
                $output .= "<p>" . $pexcerpt . "</p>";
            $output .= "</div>";
            
            
        endwhile;
        $output .= "</div>";
        wp_reset_postdata();
    endif;
    $output .= "</div>";
    echo $output;

}